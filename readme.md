# Restauracja

Prosta apka w Javie na zalicznie. Front zrobiony w PrimeFaces. Obsługuje stoliki, zamówienia oraz proste statystyki. Korzysta z spring boot'a. Repo jest wyciągnięte https://github.com/EwaPie/Projekt którego byłem współautorem(w folderze JAVA/resturacja znajdują się odpowiednie pliki). 